# API proto

grpc公共的 proto API;

## run更新

```shell 
#安装protobuf
cd proto
./protoc.sh

# 安装golang for protobuf插件
go get github.com/golang/protobuf/protoc-gen-go
go get -u -v github.com/golang/protobuf/protoc-gen-go
```

## install

go mod tidy

## test

通过客户端测试

## 使用

其它项目：go get github.com/gs-mblock/k8sgrpc-proto

## error

- ./proto/protoc.sh: line 8: protoc: command not found

```shell script
brew install protobuf
protoc  --version
```

- protoc-gen-go: program not found or is not executable

1. 打开 【终端】运行;
2. 检查 echo $PATH ;检查 .bash_profile文件；
3. source ~/.bash_profile

## mocks

测试有好多问题，暂时放弃

```shell
./mockgen -destination ./mocks/user/user_v1.go -package user -source ./proto/user/user.pb.go

./mockgen -destination ./mocks/mock_helloworld/hw_mock.go -package mock_helloworld -source ./proto/helloworld/helloworld.pb.go
```
