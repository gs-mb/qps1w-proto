Basepath=$(cd `dirname $0`; pwd)
#echo ${Basepath}
echo "---start---"
##############################

# demo
cd ${Basepath}/demo
protoc --go_out=plugins=grpc:. *.proto
echo "---demo finish---"

# helloworld
cd ${Basepath}/helloworld
protoc --go_out=plugins=grpc:. *.proto
echo "---helloworld finish---"

# user
cd ${Basepath}/user
protoc --go_out=plugins=grpc:. *.proto
echo "---user finish---"

# mq
cd ${Basepath}/mq
protoc --go_out=plugins=grpc:. mq.proto
echo "---mq finish---"

# 
echo "---all finish---"
# end